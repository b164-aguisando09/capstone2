const Service = require("../models/service");



//create new service
module.exports.addService = (reqBody, userData) => {

	return Service.findOne({userId: userData.id}).then(result => {
		if (result == null) {

			let newService = new Service({
				userId: userData.id,
				image: reqBody.image,
				nickName: reqBody.nickName,
				description: reqBody.description,
				hobby: reqBody.hobby,
				pricePerHour: reqBody.pricePerHour

			})

			return newService.save().then((service, error) => {
				if (error) {
					return false;
				} else{
					return true;
				}
			})

		} else {
			//service profile already existing
			console.log("existing service")
			return false;
		}
	})

	
}


//retrieve all services
module.exports.getAllServices = () => {
	return Service.find({}).then(result => {
		return result;
	})
}

//get all active services
module.exports.getActiveServices = () => {
	return Service.find({ isActive: true}).then(result => {
		return result;
	})
}

//get specific service using service id

module.exports.getService = (serviceId) => {
	return Service.findById(serviceId).then(result => {
		return result;
	})
}


//update a service
module.exports.updateService = (reqBody, userData) => {

	return Service.findOne({userDetails: userData.id}).then(service => {
		console.log(service);

		services = {

		nickName: service.nickName,
		description: service.description,
		hobby: service.hobby,
		pricePerHour: reqBody.pricePerHour
		}

		return service.save().then((services, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	})

/*	return Service.findAndUpdate({userDetails: userData.id}, updatedService).then((service, error) => {

		console.log(service);
		if (error) {
			return false;

		} else{
			return true;
		}
	})*/
}



//soft delete a service
module.exports.archiveService = (serviceId) => {

	let archive = {
		isActive: false
	}
	
	return Service.findByIdAndUpdate(serviceId, archive).then((service, error) => {
		if (error) {
			return false;

		} else{
			return true;
		}
	})
}

//check my service profile

module.exports.getServiceProfile = (userData) => {

	return Service.find({userId: userData.id}).then(result => {
		return result;
	})
}

//check my customers

module.exports.getCustomers = (userData) => {

	return Service.findOne({userId: userData.id}).then((result,error) => {
		if (error) {
			return false;
		} else {

			let customers = [];

			customers = result.customers;
			return customers;
		}

	})
}