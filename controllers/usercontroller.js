const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Service = require('../models/service');
const Transaction = require('../models/transaction');

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		}
	})
}

//sign up
module.exports.registerUser = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.email === reqBody.email) {
			return false;
		} else {

			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				address: {
					city: reqBody.city,
					province: reqBody.province
				},
				mobileNo: reqBody.mobileNo,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((user, error) => {
				if (error) {
					return false;

				} else{
					return true;
				}
			})
		}
	})
	
}

//User authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}else{
				return false;
			}
		}
	})
}


module.exports.getUserDetails = (userData) => {
	return User.findById(userData.id).then((result,error) => {
		if (error) {
			return false;
		} else{

			result.password = "";

			return result;
		}
	})
}

module.exports.createProvider = (userId) => {

	let update = {isServiceProvider: true};

	return User.findByIdAndUpdate(userId, update).then((result,error) => {
		if (error) {
			return false;
		} else{

			return true;
		}
	})
}

module.exports.getAllUserDetails = () => {
	return User.find().then((result,error) => {
		if (error) {
			return false;
		} else{

			result.password = "";

			return result;
		}
	})
}


//avail service

module.exports.availService = async (userData, reqBody) => {

	let myDate = new Date(reqBody.date);
	let myDateToString = myDate.toString();
	let serviceId = reqBody.serviceId;
	let numberOfHours = reqBody.numberOfHours;

	let isServiceAvailable = await Service.findById(serviceId).then(service => {

		let customers = [];
		customers = service.customers;
		if (customers.length == 0) {
			return true;
		} else if(customers.length >= 1){
			for (let i = 0; i < customers.length; i++){

				let availedDate = customers[i].availedOnDate.toString();
				console.log(myDateToString);
				console.log(availedDate);
				console.log(customers.length);
				console.log(i);

				if (availedDate == myDateToString) {
					return false;
					break;
				} else if (i == customers.length-1) {
						return true;
					}
			} 
		}
		
	});


	if (isServiceAvailable) {

		let isUserUpdated = await User.findById(userData.id).then(user => {
	
				user.serviceAvail.push({
					serviceId: reqBody.serviceId,
					availedOn: myDate

				});

				return user.save().then((user, error) => {
					if (error) {
						return false;
					} else{
						return true;
					}
				})
				
		});

		let isServiceUpdated = await Service.findById(serviceId).then(service => {

			let pricePerHour = service.pricePerHour;
			let totalPrice = numberOfHours * pricePerHour;
			let customers = [];

			customers = service.customers;

			let newTransaction = new Transaction({
				userDetails: {
					userId: userData.id
				},

				availedService: {
					serviceId: serviceId
				},

				duration: numberOfHours,
				pricePerHour: pricePerHour,
				totalPayment: totalPrice

			});

			newTransaction.save()

						customers.push({

							userId: userData.id,
							availedOnDate: myDate,
							availedOnTime: reqBody.time,
							totalPayment: totalPrice
						})

						return service.save().then((service, error) => {
							if (error) {
								return false;
							} else{
								return true;
							}
						})
		});

		if (isUserUpdated && isServiceUpdated) {
			return true;
		} else {
			return false;
		}

	} else{
		return false;
	} 
}

//check all availed services


module.exports.getAllAvailedServices = (userData) => {
	return User.findById(userData.id).then((result,error) => {
		if (error) {
			return false;
		} else{

			return result.serviceAvail;
		}
	})
}

//check all my transactions
//update data in user and transaction model

module.exports.getAllTransactions = async (userData) => {
	
	let transactionData = await Transaction.find({'userDetails.userId': userData.id}).then((transaction,error) => {
		if (error) {
			return false;
		} else{
		
			return transaction;
		}
	})

	if (transactionData.length == 0) {
		return transactionData;
	} else{

		let arrayId = await Transaction.find({'userDetails.userId': userData.id}).then((transaction,error) => {
			if (error) {
				return false;
			} else{
				let arrayId = transaction.map(function(trans) {
					return trans._id;
				})

				console.log(arrayId);
				return arrayId;
			}
		})

		let transactionId = await User.findById(userData.id).then(user => {
			let transactionPayment = [];
			transactionPayment = user.transactionPayment;
		if (transactionPayment.length == 0) {
			return arrayId[0]
		} else {

			let trans = transactionPayment.map(result => {
				return result.transactionId;
			})
			for (let n = 0; n < arrayId.length; n++){
				let transId = arrayId[n];
				

				if (n == arrayId.length-1) {
					for (let i = 0; i < trans.length; i++){
						let transaction = trans[i];
						if (transaction == transId) {
							return null;
							break;
						} else if (i == trans.length-1) {
								return transId;
							}
					} 
				} else {

					if (trans.length == 0) {
						return transId;
					} else if(trans.length >= 1){
						for (let i = 0; i < trans.length; i++){
							let transaction = trans[i];
							if (transaction == transId) {
								break;
							} else if (i == trans.length-1) {
									return transId;
								}
						} 
					}
				}
			}
		}
			
		})

		console.log(transactionId);
		let isTransactionSaved = await User.findById(userData.id).then(user => {
			let transactionPayment = [];
			transactionPayment = user.transactionPayment;
		if (transactionPayment.length == 0) {
			return false;
		} else {
			let trans = transactionPayment.map(result => {
				return result.transactionId;
			})
			
			if (trans.length == 0) {
				return false;
			} else if(transactionId == null){
				return true;
			} else{
				for (let i = 0; i < trans.length; i++){
					let transaction = trans[i]
					if (transaction == transactionId) {
						return true;
						break;
					} else if (i == trans.length-1) {
							return false;
						}
				} 
			}
		}
			 
		})

		console.log(isTransactionSaved);
		if (isTransactionSaved) {
			return transactionData;
		} else {
			let transSaveToUser = await User.findById(userData.id).then(user => {
				let transaction = [];
				transaction = user.transactionPayment;
				let id = transactionId;
				transaction.push({
					transactionId: id,
					date: new Date(),
					status: "pending"
				})

				return user.save().then((user, error) => {
					if (error) {
						return false;
					} else{
						return true;
					}
				})

			})

			if (transSaveToUser) {
				return transactionData;
			} else {
				return false;
			}
		}
	}


}

//pay specific transaction
/*

module.exports.paySpecificTransaction = async (userData, id) => {

	let updatedTransaction = {isPaid: true};
	let isTransactionUpdated = await Transaction.findByIdAndUpdate(id, updatedTransaction).then((result,error) => {
		if (error) {
			return false;
		} else{
			return true;
		}
	})

	let isUserUpdated = await User.find({"transactionPayment.transactionId": id}, {transactionPayment: 1}).then(result => {
		let transactionPay = [];

		transactionPay = result[0].transactionPayment
		console.log(transactionPay);

		for (let i = 0; i < transactionPay.length; i++){

			let transactionId = transactionPay[i].transactionId;
			if (transactionId == id) {
				transactionPay[i] = {
					transactionId: transactionId,
					status: "Paid",
					date: new Date()

				}

				return result[0].save().then((user, error) => {
					if (error) {
						return false;
					} else{
						return true;
					}
				})
			}

		}
	})

	if (isTransactionUpdated && isUserUpdated) {
		return true;
	} else {
		return false;
	}
}


//check transaction payment

module.exports.getSpecificTransactionPrice = (id) => {
	return Transaction.findById(id).then((result,error) => {
		if (error) {
			return false;
		} else{
			console.log(result.totalPayment);
			price = result.totalPayment
			return price.toString();
		}
	})
}
*/

//check total amount of unpaid transactions

module.exports.getTotalUnpaid = (userData) => {
	return Transaction.find({'userDetail.userId': userData.Id}).then((result,error) => {
		if (error) {
			return false;
		} else{
			let x = 0;
			let totalUnpaid = 0;

			for (let i = 0; i < result.length; i++){
				let isPaid = result[i].isPaid;
				let totalPayment = result[i].totalPayment

				if (isPaid != true) {
					

					totalUnpaid = x + totalPayment;

					x = totalUnpaid;
				}
			}

			return totalUnpaid.toString()
		}
	})
}
