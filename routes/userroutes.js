const express = require('express');
const router = express.Router();

const auth = require("../auth");

const userController = require("../controllers/usercontroller");




//find duplication
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result))
});


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
});

//check account
router.get("/userDetails", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	

	userController.getUserDetails(userData).then(result => res.send(result))
} );

//make the user to a service provider (admin)
router.put("/createServiceProvider/:userId", (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	

	if (isAdmin) {
		userController.createProvider(req.params.userId).then(result => res.send(result))
	} else {

		res.send(false);
	}
})

//get all user details
router.get("/allUsers", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if (isAdmin) {
		
		userController.getAllUserDetails().then(result => res.send(result));

		
	} else{
		res.send("not an admin");
	}

} );

//avail a service (customer)

router.put("/availService", auth.verify, (req, res) => {

	userData = auth.decode(req.headers.authorization)


	if (userData.isServiceProvider) {

		res.send(false);
		console.log("not a customer")
	} else {

		userController.availService(userData, req.body).then(result => res.send(result));
	}

})

//check all services availed

router.get("/checkAllAvailedServices", auth.verify, (req, res) => {

	userData = auth.decode(req.headers.authorization)

	if (userData.isServiceProvider) {

		res.send(false);
		console.log("not a customer")
	} else {

		userController.getAllAvailedServices(userData).then(result => res.send(result));
	}
})

//check all my transactions

router.put("/checkAllTransactions", auth.verify, (req, res) => {

	userData = auth.decode(req.headers.authorization)

	if (userData.isServiceProvider) {

		res.send(false);
		console.log("not a customer")
	} else {

		userController.getAllTransactions(userData).then(result => res.send(result));
	}
})

//pay specific transaction

router.put("/transaction/:transactionId", auth.verify, (req, res) => {
userData = auth.decode(req.headers.authorization)

	if (userData.isServiceProvider) {

		res.send(false);
		console.log("not a customer")
	} else {

		userController.paySpecificTransaction(userData, req.params.transactionId).then(result => res.send(result));
	}
})

//check transaction payment
router.get("/transaction/:transactionId", auth.verify, (req, res) => {
userData = auth.decode(req.headers.authorization)

	if (userData.isServiceProvider) {

		res.send(false);
		console.log("not a customer")
	} else {

		userController.getSpecificTransactionPrice(req.params.transactionId).then(result => res.send(result));
	}
})


//check total amount of unpaid transactions
router.post("/transaction/totalUnpaidAmount", auth.verify, (req, res) => {
userData = auth.decode(req.headers.authorization)

	if (userData.isServiceProvider) {

		res.send(false);
		console.log("not a customer")
	} else {

		userController.getTotalUnpaid(userData).then(result => res.send(result));
	}
})


module.exports = router;