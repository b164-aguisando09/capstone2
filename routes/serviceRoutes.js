const express = require("express");
const router = express.Router();

const auth = require("../auth");

const serviceController = require("../controllers/serviceController");


//add service (service provider)
router.post("/addService", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isServiceProvider) {

		serviceController.addService(req.body, userData).then(result => res.send(result));

	}	else{
		console.log("not a service provider")

		res.send(false);
	}

});


//get all services (admin)
router.get("/all", (req, res) => {



		serviceController.getAllServices().then(result => res.send(result));

	
});


//get all active services (admin)
router.get("/activeService", (req, res) => {

<<<<<<< HEAD
	
		
		serviceController.getActiveServices().then(result => res.send(result));

	
=======
		
		serviceController.getActiveServices().then(result => res.send(result));


>>>>>>> 14ffccc237e65eed872ae40e176530ca6a3861e4

});


<<<<<<< HEAD
//get specific service 
router.get("/:serviceId", (req, res) => {
	
		
		serviceController.getService(req.params.serviceId).then(result => res.send(result));

	

=======

router.get("/:serviceId", (req, res) => {

		
		serviceController.getService(req.params.serviceId).then(result => res.send(result));
	
>>>>>>> 14ffccc237e65eed872ae40e176530ca6a3861e4
})

//update service profile (service provider)

router.put("/updateServiceProfile", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isServiceProvider) {
		
		serviceController.updateService(req.body, userData).then(result => res.send(result))

	} else{

		res.send(false);

	}



})

//soft delete service (admin)

router.put("/:serviceId/archive", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	

	if (isAdmin) {
		serviceController.archiveService(req.params.serviceId).then(result => res.send(result))
	} else {

		res.send(false);
	}
})


//get my service profile (service provider)

router.post("/myServiceProfile", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	if (userData.isServiceProvider) {
		
		serviceController.getServiceProfile(userData).then(result => res.send(result));

	} else{
		res.send(false);
	}

})

//get my customers

router.post("/getCustomers", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	if (userData.isServiceProvider) {
		
		serviceController.getCustomers(userData).then(result => res.send(result));

	} else{
		console.log("not a service provider")
		res.send(false);
	}
})


module.exports = router;