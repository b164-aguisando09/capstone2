const mongoose = require("mongoose");

const transactionSchema = new mongoose.Schema({

	userDetails: {

			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			appliedOn: {
				type: Date,
				default: new Date()
			}
	},

	availedService: {
		serviceId: {
			type:String,
			required:[true, "Service ID is required."]
		},

		status: {
			type: String,
			default: "Pending"
		}
	},

	duration: {
		type: Number,
		required: [true, "Duration is required."]
	},

	pricePerHour: {
		type: Number,
		required: [true, "Price per hour is required"]
	},


	totalPayment: {
		type: Number,
		required: [true, "Description is required"]
	},


	isPaid: {
		type: Boolean,
		default: false
	}
})


module.exports = mongoose.model("Transaction", transactionSchema);


