const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required."]
	},

	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},

	address: {

			province: {
				type: String,
				required: [true, "Province is required."]
			},

			city: {
				type: String,
				required: [true, "City is required."]
			}
		
	},

	mobileNo: {
		type: String,
		required: [true, "mobileNumber is required."]
	},

	email: {
		type: String,
		required: [true, "Email is required."]
	},

	password: {
		type: String,
		required: [true, "password is required."]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	isServiceProvider: {
		type: Boolean,
		default: false
	},

	serviceAvail: [

		{
			serviceId: {
				type: String,
				required: [true, "Service ID is required."]
			},
			availedOn: {
				type: Date,
				required: [true, "Date is required."]
			},

			tookedOn: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: "Service availed"
			},
			isPaid: {
				type: Boolean,
				default: false
			}
		}
	],

	transactionPayment: [
		{
			transactionId: {
				type: String,
				required: [true, "Transaction Payment is required."]
			},

			date: {
				type: Date,
				required: [true, "Date is required."]
			},

			status: {
				type: String,
				default: "Paid"
			}
		}
	]
})



module.exports = mongoose.model("User", userSchema);