const mongoose = require("mongoose");

const serviceSchema = new mongoose.Schema({

	userId: { 

		type: String,
		required: [true, "UserId is required"]
			
		},

	nickName: {
		type: String,
		required: [true, "Nickname is required."]
	},

	image: {
		type: String,
		required: [true, "Image is required."]
	},

	description: {
		type: String,
		required: [true, "Description is required"]
	},

	hobby: {
		type: String,
		required: [true, "Hobby is required."]
	},

	pricePerHour: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	isAvailable: {
		type: Boolean,
		default: true
	},


	customers: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required."]
			},
			availedOnDate: {
				type: Date,
				required: [true, "Date is required."]
			},
			availedOnTime: {
				type: String,
				required: [true, "Time is required"]
			},

			totalPayment: {
				type: Number,
				required: [true, "totalPayment is required."]
			}
		}

	]



})


module.exports = mongoose.model("Service", serviceSchema);



